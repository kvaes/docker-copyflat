#!/bin/bash

LOCK=/tmp/copyflat.lock

if [ ! -f $LOCK ]
then
  echo "no lock, setting lock"
  touch $LOCK
else
  echo "locked, exiting"
  exit 1
fi

find /data/in -name "*.nfo" -type f -delete >/dev/null 2>&1
find /data/in -name "*.txt" -type f -delete >/dev/null 2>&1
find /data/in -type f -print0 | xargs -0 mv -t /data/out >/dev/null 2>&1
find /data/in -type d -empty -delete >/dev/null 2>&1

echo "unlocking"
rm $LOCK
